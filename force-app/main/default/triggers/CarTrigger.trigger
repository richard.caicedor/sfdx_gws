trigger CarTrigger on car__c (before insert, before update) {

    if(Trigger.isInsert){
        for(car__c c: Trigger.New){
            c.Name =  c.Name + ' ' + c.Model_Year__c + ' ' + c.Registration_Plate__c;
            c.Full_Price__c = c.Price__c + c.Tax__c;
        }
    }else if(Trigger.isUpdate){
        for(car__c c: Trigger.New){
            c.Full_Price__c = c.Price__c + c.Tax__c;
        }
    }
}