@RestResource(urlMapping='/Leads/*')
global with sharing class LeadRestResource {

    @HttpGet
    global static List<Lead> listIdGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<Lead> result = null;
        if(req.params.size() > 0){
            String IdLead = req.params.values()[0];
            result = [SELECT Name, Company, Email, Industry, City, Phone FROM Lead WHERE Id = :IdLead ];
        }else{
            result = [SELECT Name, Company, Email, Industry, City, Phone FROM Lead];
        }
        return result;
    } 

    
  
}