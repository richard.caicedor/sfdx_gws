public class CallApiGWS {
    
    public class Respuesta{
        public string status;
        public string labor_rate;
        public string description;
    }

    @future(callout=true)
    public static void getLaborRate(String IdLead){
        HttpRequest request = new HttpRequest();
        Lead objLead = [SELECT Address, Company, CleanStatus, Email  FROM Lead WHERE Id = :IdLead LIMIT 1];
        Address add = objLead.Address;
        String codePostal = String.valueOf(add.postalcode);
        String endpoint = 'https://agsdev-ags.cs10.force.com/services/apexrest/v1/carsApi?zipcode='+codePostal;
        request.setEndPoint(endpoint);
        request.setMethod('GET');
        request.setHeader('api-key', 'Ow8VcQTZTCWFsQYAUOjz');
        Http http = new Http();
        HTTPResponse res = http.send(request); 
        Respuesta results = (Respuesta) JSON.deserialize(res.getBody(), Respuesta.class);
        if (results.status == 'Success') {
            String labor_rate = results.labor_rate;
            objLead.LaborRate__c = Decimal.valueOf(labor_rate); 
            update objLead;
        }else{
            System.debug('Error: '+results.description);
        }
    }
}


