public class LeasingCalculator {

    public car__c datacar {get; set;}
    private car__c car;

    public LeasingCalculator( ApexPages.StandardController stdController ) {
        this.car = (car__c)stdController.getRecord();
        datacar = new car__c();
        datacar.Id = this.car.Id;
    } 

    public void Calculator(){
        update datacar;
    }
}
