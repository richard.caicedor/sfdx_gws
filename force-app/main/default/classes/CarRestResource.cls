@RestResource(urlMapping='/Cars/*')
global with sharing class CarRestResource {
 
    @HttpGet
    global static List<car__c> getCarsIdLead(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<car__c> result = null;
        if(req.params.size() > 0){
            String IdLead = req.params.values()[0];
            result = [SELECT Id, Name, Registration_Plate__c, Full_Price__c, Model_Year__c, Leasing__c, Price__c, Tax__c,Lead__c FROM car__c WHERE Lead__c = :IdLead ];
        }else{
            result = [SELECT Id, Name, Registration_Plate__c, Full_Price__c, Model_Year__c, Leasing__c, Price__c, Tax__c,Lead__c FROM car__c];
        }
        return result;  
    }

    @HttpPost
    global static car__c postCreateCar(car__c objCar){ 
        insert objCar;
        return objCar; 
    }
}