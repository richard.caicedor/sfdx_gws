public class CarSearchController {

    List<car__c> objCars;   

    public CarSearchController() {
        objCars = new List<car__c>();
    }

    public List<car__c> getObjCars()
    {
        String item = '';
        if(String.isNotBlank(item)){
            List<List<SObject>> result = [
                FIND: item
                RETURNING car__c(
                    Name, Full_Price__c, Leasing__c, Price__c, Tax__c, Registration_Plate__c, Down_Payment__c
                )
            ];
            objCars = result[0];
        }else{
            objCars = [SELECT Name, Full_Price__c, Model_Year__c, Leasing__c, Price__c, Tax__c, Registration_Plate__c, Down_Payment__c FROM car__c];
        }
        return objCars;
    }
}
